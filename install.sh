#!/bin/bash
set -e

if ! [ $(id -u) = 0 ]; then
    SUDO=sudo
else
    SUDO=
fi
HOSTNAME=$(cat /etc/hostname)
WD=$(pwd -P)

command_exists() {
    command -v $1 2>/dev/null ;
}

# Add repositories
if [ ! -f /etc/apt/sources.list.d/google-chrome.list ]; then
    $SUDO echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > \
        /etc/apt/sources.list.d/google-chrome.list
    $SUDO apt-key adv --keyserver keyserver.ubuntu.com \
        --recv-keys EB4C1BFD4F042F6DDDCCEC917721F63BD38B4796
fi

echo "Updating software"
$SUDO apt-get update
$SUDO apt-get dist-upgrade -y

echo "Installing new software"
$SUDO apt-get install -y \
    build-essential \
    cmake \
    compton \
    curl \
    deluge-gtk \
    firmware-amd-graphics \
    firmware-iwlwifi \
    firmware-realtek \
    google-chrome-stable \
    gimp \
    git \
    htop \
    i3-wm \
    imagemagick \
    inkscape \
    keepassxc \
    libasound2-dev \
    libbz2-dev \
    libcairo2-dev \
    libfreetype6-dev \
    libfontconfig1-dev \
    libjsoncpp-dev \
    libnl-genl-3-dev \
    libpulse-dev \
    libreadline-dev \
    libssl-dev \
    libsqlite3-dev \
    libxcb1-dev \
    libxcb-composite0-dev \
    libxcb-ewmh-dev \
    libxcb-icccm4-dev \
    libxcb-image0-dev \
    libxcb-randr0-dev \
    libxcb-util0-dev \
    libxcb-xfixes0-dev \
    lightdm \
    lightdm-gtk-greeter \
    maim \
    mpv \
    neovim \
    pkg-config \
    python3 \
    python3-pip \
    python3-sphinx \
    python-xcbgen \
    redshift \
    rofi \
    rsync \
    thunderbird \
    tmux \
    wireless-tools \
    xcb-proto \
    xscreensaver \
    xss-lock \
    xsecurelock \
    youtube-dl \
    zathura

# Set up Qualia
if ! command_exists qualia ; then
    $SUDO pip3 install mir.qualia
fi

if [[ $(git config --get filter.qualia.clean | head -c1 | wc -c) -eq 0 ]]; then
    echo "Setting up qualia for this repository"
    git config filter.qualia.clean qualia
    git config filter.qualia.smudge "qualia $HOSTNAME"
    rm .git/index
    git checkout HEAD -- "$(git rev-parse --show-toplevel)"
fi

# Set up git
mkdir -p $HOME/.git
ln -vfs $WD/global-gitignore $HOME/.git/global-gitignore
git config --global core.excludesfile '$HOME/.git/global-gitignore'
git config --global user.email 'brontitall@dds.nl'
git config --global user.name 'Xeryus Stokkel'

# Set up git prompt
if [ ! -d $HOME/.bash-git-prompt ]; then
    git clone https://github.com/magicmonty/bash-git-prompt.git $HOME/.bash-git-prompt --depth=10
fi

# Set up Xorg
# BEGIN mantaray
$SUDO ln -vfs $WD/mantaray/monitor.conf /etc/X11/xorg.conf.d/20-monitor.conf
# END mantaray

# Set up i3
if [ ! -d $HOME/.i3 ]; then
    echo "Symlinking i3 config"
    mkdir -p $HOME/.i3
    ln -vfs $WD/i3/config $HOME/.i3/config
    ln -vfs $WD/i3/exit.sh $HOME/.i3/exit.sh
    ln -vfs $WD/i3/lock.sh $HOME/.i3/lock.sh
fi

# Set up rust
if ! command_exists cargo; then
    ./rustup.sh --default-toolchain stable --no-modify-path -y -c rust-src
fi

# Install alacritty
if ! command_exists alacritty; then
    echo "Installing alacritty"
    git clone https://github.com/jwilm/alacritty.git $HOME/alacritty
    cd $HOME/alacritty
    cargo build --release
    cargo install cargo-deb
    $SUDO cargo deb --install -p alacritty
    cd $WD
fi

# Install polybar
if ! command_exists polybar; then
    echo "Installing polybar"
    git clone --recursive https://github.com/polybar/polybar.git $HOME/polybar
    cd $HOME/polybar
    mkdir -p build
    cd build
    cmake ..
    make -j4
    $SUDO make install
    cd $WD

fi
# Create config
mkdir -p $HOME/.config/polybar
ln -vfs $WD/polybar/launch.sh $HOME/.config/polybar/launch.sh
ln -vfs $WD/polybar/system-cpu-loadavg.sh \
    $HOME/.config/polybar/system-cpu-loadavg.sh
chmod +x $HOME/.config/polybar/launch.sh
# BEGIN mantaray
ln -vfs $WD/mantaray/polybar/config $HOME/.config/polybar/config
# END mantaray

mkdir -p \
    $HOME/screenshots \
    $HOME/.config/rofi
ln -vfs $WD/alacritty.yml $HOME/.config/alacritty.yml
ln -vfs $WD/bashrc $HOME/.bashrc
ln -vfs $WD/redshift.conf $HOME/.config/redshift.conf
ln -vfs $WD/rofi/config.rasi $HOME/.config/rofi/config.rasi
ln -vfs $WD/rofi/solarized_alternate.rasi $HOME/.config/rofi/solarized_alternate.rasi
ln -vfs $WD/rofi/greenclip.toml $HOME/.config/

# Install pyenv
if ! command_exists pyenv; then
    git clone --depth=2 https://github.com/pyenv/pyenv.git $HOME/.pyenv
    git clone --depth=2 https://github.com/pyenv/pyenv-virtualenv.git \
    	$HOME/.pyenv/plugins/pyenv-virtualenv
fi

# Enable pyenv so that vim can be set up
export PYENV_ROOT="$HOME/.pyenv/"
if command_exists pyenv; then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi

# Set up neovim
if [ ! -d $HOME/.config/nvim ]; then
    mkdir -p $HOME/.config/nvim
fi
ln -vfs $WD/neovim/init.vim $HOME/.config/nvim/init.vim
if [ ! -f $HOME/.local/share/nvim/site/autoload/plug.vim ]; then
    curl -fLo $HOME/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

# Python venvs for use with neovim
if [ ! -d $PYENV_ROOT/versions/3.7.6 ]; then
    $PYENV_ROOT/bin/pyenv install 3.7.6
fi
if [ ! -d $PYENV_ROOT/versions/neovim3 ]; then
    $PYENV_ROOT/bin/pyenv virtualenv 3.7.6 neovim3
    $PYENV_ROOT/versions/neovim3/bin/pip install --upgrade neovim jedi
fi

# Rust completions for neovim
rust_nightly_chain=$(rustup toolchain list | grep -c nightly || true)
if [[ $rust_nightly_chain -eq 0 ]]; then
    rustup toolchain add nightly
fi

if ! command_exists racer; then
    cargo +nightly install racer
fi

# Install greenclip
if ! command_exists greenclip; then
    curl -fLo /tmp/greenclip \
        https://github.com/erebe/greenclip/releases/download/4.2/greenclip
    $SUDO mv /tmp/greenclip /usr/local/bin/greenclip
    $SUDO chmod +x /usr/local/bin/greenclip
    if [ ! -d $HOME/.config/systemd/user ]; then
    	mkdir -p $HOME/.config/systemd/user
    fi
    ln -vfs $WD/rofi/greenclip.service $HOME/.config/systemd/user/greenclip.service
    systemctl --user enable greenclip.service
    systemctl --user start greenclip.service
fi

# Install keybase
if ! command_exists run_keybase; then
    curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb
    $SUDO apt install ./keybase_amd64.deb
    rm keybase_amd64.deb
fi

# Install Firefox
if ! command_exists firefox; then
    wget -O FirefoxSetup.tar.bz2 "https://download.mozilla.org/?product=firefox-latest&os=linux64&lang=en-US"
    $SUDO tar xjf FirefoxSetup.tar.bz2 -C /opt
    rm FirefoxSetup.tar.bz2
    $SUDO ln -s /opt/firefox/firefox /usr/local/bin/firefox
fi

# Install screensaver
if [ ! -d $HOME/kittenstream ]; then
    git clone https://gitlab.com/XeryusTC/kittenstream.git $HOME/kittenstream
    cd $HOME/kittenstream
    $PYENV_ROOT/bin/pyenv virtualenv 3.7.6 kittens
    $PYENV_ROOT/versions/kittens/bin/pip install requests==2.22.0
    cd $WD
fi


# Install SpiderOak
if ! command_exists SpiderOakONE; then
    curl -fLo $HOME/spideroak_amd64.deb https://spideroak.com/release/spideroak/deb_x64
    $SUDO dpkg -i $HOME/spideroak_amd64.deb
fi
if [ ! -f $HOME/.config/SpiderOakONE/config.txt ]; then
    read -sp "SpiderOak password: " spideroakpass
    read -p "spiderOak device name: " spideroakname
    echo {\"username\":\"XeryusTC\",\"password\":\"${spideroakpass}\",\
    	\"device_name\":\"${spideroakname}\",\"reinstall\":false} > /tmp/spideroak.json
    SpiderOakONE --setup=/tmp/spideroak.json
    rm /tmp/spideroak.json
fi
